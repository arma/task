**Address book**

__Install__  
1. `git clone git@bitbucket.org:arma/task.git`  
2. `composer install` - This will also create `database.sqlite` database in `database/`  
3. `php artisan migrate --seed` - Generates some uses using factories  

---

_Preview_

[http://task.blackarma.com](http://task.blackarma.com), [http://task.blackarma.com/login](http://task.blackarma.com/login)

_Use generated login emails and password **secret**_

Frontend uses static blade templating (on additional request can do vue.js)  
No tests added (on additional request can do any)  
User class, entity, auth was taken from default framework scaffold.  
Restructured for some DDD (repositories base + shipping, entities)

Thanks.