<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Client part
Route::group(['prefix' => 'client'], function() {

    // Anything with addresses
    Route::group(['prefix' => 'address', 'middleware' => ['auth']], function() {

        Route::get('create', 'Shipping\ShippingAddressController@create')->name('get.create.shipping.address');
        Route::post('store', 'Shipping\ShippingAddressController@store')->name('post.store.shipping.address');

        Route::get('edit/{internalId}', 'Shipping\ShippingAddressController@edit')->name('get.edit.shipping.address');
        Route::post('update', 'Shipping\ShippingAddressController@update')->name('post.update.shipping.address');
        Route::get('update/{internalId}/{default?}', 'Shipping\ShippingAddressController@quickUpdate')->name('get.update.shipping.address');

        Route::get('delete/{internalId}', 'Shipping\ShippingAddressController@delete')->name('get.delete.shipping.address');

    });

});


