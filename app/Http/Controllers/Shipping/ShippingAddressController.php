<?php

namespace App\Http\Controllers\Shipping;

use App\Http\Controllers\HomeController;
use App\Http\Requests\CreateShippingAddressRequest;
use App\Http\Requests\ShippingAddressRequest;
use App\Repositories\ShippingAddressRepository;
use Illuminate\Contracts\Auth\Factory as Auth;

class ShippingAddressController extends HomeController
{
    public $shippingAddressRepository;
    private $auth;


    public function __construct(ShippingAddressRepository $shippingAddressRepository, Auth $auth)
    {
        $this->middleware('auth');
        $this->shippingAddressRepository = $shippingAddressRepository;
        $this->auth = $auth;
    }

    /**
     * Display view of adding new shipping address
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $user = $this->auth->user();
        $shippingAddressesCount = $user->shippingAddresses->count();

        if ($shippingAddressesCount >= 3) {
            return redirect('home')->with('error', trans('shipping.address limit reached'));
        }

        return view('shipping.add_address', compact('shippingAddressesCount'));
    }

    /**
     * Store new shipping address
     *
     * @param ShippingAddressRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ShippingAddressRequest $request)
    {
        $user = $this->auth->user();
        $shippingAddress = $user->shippingAddresses;
        $shippingAddressesCount = $shippingAddress->count();

        $default = (int) isset($request->default);
        if ($shippingAddressesCount === 0) {
            $default = 1;
        }

        $usableInternalIds = [1];
        if ($shippingAddressesCount >= 1) {
            // Figure out next available internalId
            $takenInternalIds = $shippingAddress->pluck('internal_id')->toArray();
            $allInternalIds = range(1, max($takenInternalIds)+1);
            $usableInternalIds = array_diff($allInternalIds, $takenInternalIds);
        }

        $internalId = reset($usableInternalIds);
        $shippingAddress = $this->shippingAddressRepository->create(
            array_merge(
            [
                'user_id' => $user->id,
                'internal_id' => $internalId,
                'default' => $default,
            ],
            $request->all()
            )
        );

        // Clean up defaults
        if (!empty($default) && $shippingAddress !== null) {
            $this->shippingAddressRepository->cleanDefaults($user->id, $shippingAddress->internal_id);
        }

        return redirect('home')->with('success', trans('shipping.address added'));
    }

    /**
     * Display edit view for shipping addresses
     * @param int $internalId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($internalId)
    {
        $userId = $this->auth->user()->id;

        $shippingAddress = $this->shippingAddressRepository->findByInternalId($userId, $internalId);
        $shippingAddressesCount = $this->shippingAddressRepository->countForUser($userId);

        return view('shipping.edit_address', compact('shippingAddress', 'shippingAddressesCount'));
    }

    public function update(ShippingAddressRequest $request)
    {
        $userId = $this->auth->user()->id;

        $shippingAddress = $this->shippingAddressRepository->findByInternalId($userId, $request->internal_id);
        if ($shippingAddress !== null) {
            $shippingAddress->update($request->all());
            if (!empty($shippingAddress->default)) {
                $this->shippingAddressRepository->cleanDefaults($userId, $shippingAddress->internal_id);
            }
        }

        return redirect('home')->with('success', trans('shipping.address updated'));
    }

    public function quickUpdate($internalId)
    {
        $userId = $this->auth->user()->id;

        $shippingAddress = $this->shippingAddressRepository->findByInternalId($userId, $internalId);
        if ($shippingAddress !== null) {
            $shippingAddress->update(['default' => 1]);
            $this->shippingAddressRepository->cleanDefaults($userId, $internalId);

            return redirect('home')->with('success', trans('shipping.address default updated'));
        }

        return redirect('home')->with('error', trans('shipping.address update failed'));
    }

    public function delete($internalId)
    {
        $userId = $this->auth->user()->id;

        // Get record
        $shippingAddress = $this->shippingAddressRepository->findByInternalId($userId, $internalId);
        if ($shippingAddress !== null) {
            if ($shippingAddress->default == 0) {
                $shippingAddress->delete();
                return redirect('home')->with('success', trans('shipping.address deleted'));
            }
            return redirect('home')->with('error', trans('shipping.address delete default'));
        }

        return redirect('home')->with('error', trans('shipping.address delete failed'));
    }
}
