<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Auth\Factory as Auth;

class HomeController extends Controller
{
    private $maxShippingAddresses = 3;
    private $auth;

    /**
     * Create a new controller instance.
     *
     * @param Auth $auth
     */
    public function __construct(Auth $auth)
    {
        $this->middleware('auth');
        $this->auth = $auth;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get authenticated user
        $user = $this->auth->user();

        // Get users shipping addresses
        $shippingAddresses = $user->shippingAddresses()->orderBy('default', 'DESC')->get();

        // Maximal addresses allowed
        $maxShippingAddresses = $this->maxShippingAddresses;

        return view('home', compact('user', 'shippingAddresses', 'maxShippingAddresses'));
    }
}
