<?php

namespace App\Repositories\Eloquent;

use App\Repositories\ShippingAddressRepository;
use Illuminate\Database\Eloquent\Model;

class EloquentShippingAddressRepository extends EloquentBaseRepository implements ShippingAddressRepository
{
    /**
     * Find all shipping addresses for specific user by internal id
     *
     * @param int $userId
     * @param int $internalId
     * @return Model
     */
    public function findByInternalId($userId, $internalId)
    {
        return $this->model->forUser($userId)->where('internal_id', $internalId)->first();
    }

    /**
     * Clean all default address markers for specific user except one internal id
     *
     * @param int $userId
     * @param int|null $exceptInternalId
     * @return int
     */
    public function cleanDefaults($userId, $exceptInternalId = null)
    {
        return $this->model->forUser($userId)->where('internal_id', '!=', $exceptInternalId)->update(['default' => 0]);
    }

    /**
     * Count addresses for a user
     *
     * @param int $userId
     * @return int
     */
    public function countForUser($userId)
    {
        return $this->model->forUser($userId)->count();
    }

    /**
     * Return relationship of user
     *
     * @return Model
     */
    public function user()
    {
        return $this->model->user()->first();
    }
}
