<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

interface ShippingAddressRepository extends BaseRepository
{
    /**
     * Find all shipping addresses for specific user by internal id
     *
     * @param int $userId
     * @param int $internalId
     * @return Model
     */
    public function findByInternalId($userId, $internalId);

    /**
     * Clean all default address markers for specific user except one internal id
     *
     * @param int $userId
     * @param int|null $exceptInternalId
     * @return int
     */
    public function cleanDefaults($userId, $exceptInternalId);

    /**
     * Return relationship of user
     *
     * @param int $userId
     * @return int
     */
    public function countForUser($userId);

    /**
     * Return relationship of user
     *
     * @return Model
     */
    public function user();
}
