<?php

namespace App\Providers;

use App\Entities\ShippingAddress;
use App\Repositories\Eloquent\EloquentShippingAddressRepository;
use App\Repositories\ShippingAddressRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ShippingAddressRepository::class, function () {
            return new EloquentShippingAddressRepository(new ShippingAddress());
        });
    }
}
