<?php

return [
    'add new address' => 'Add shipping address',
    'add new address button' => 'Save address',
    'address added' => 'Successfully added new shipping address',

    'edit address' => 'Edit shipping address',
    'edit address button' => 'Update address',
    'address updated' => 'Successfully updated shipping address',
    'address default updated' => 'Successfully updated default shipping address',
    'address update failed' => 'Failed to updated default shipping address',

    'delete address' => 'Delete shipping address',
    'delete address button' => 'Delete address',
    'address deleted' => 'Successfully deleted shipping address',
    'address delete default' => 'You can not delete default shipping address, please set/add another address as default first',
    'address delete failed' => 'Could not delete shipping address, please try again',

    'address limit reached' => 'You have reached available shipping address limit, please delete older addresses to add new ones',
];
