@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @if(session('success'))
        <div class="col-md-8 col-md-offset-2">
            <div class="alert alert-success">{{ session('success') }}</div>
            <p class="bg-success"></p>
        </div>
        @endif
        @if(session('error'))
            <div class="col-md-8 col-md-offset-2">
                <div class="alert alert-danger">{{ session('error') }}</div>
                <p class="bg-success"></p>
            </div>
        @endif
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Welcome {{ $user->fullName }}
                    <div class="pull-right">Your shipping addresses ({{ $shippingAddresses->count() }}/<strong>{{ $maxShippingAddresses }}</strong>)</div>
                </div>

                <div class="panel-body">
                    @if($shippingAddresses->count() < 3)
                        @include('shipping.partials.control_buttons')
                    @endif

                    @if($shippingAddresses->isNotEmpty())
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Country</th>
                                <th>City</th>
                                <th>Zip code</th>
                                <th>Street</th>
                                <th>Edit</th>
                                <th>Delete</th>
                                <th>Make default</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($shippingAddresses as $shippingAddress)
                                <tr>
                                    <td>{{ $shippingAddress->country }}</td>
                                    <td>{{ $shippingAddress->city }}</td>
                                    <td>{{ $shippingAddress->zip_code }}</td>
                                    <td>{{ $shippingAddress->street }}</td>
                                    <td><a href="{{ route('get.edit.shipping.address', ['id' => $shippingAddress->internal_id]) }}">Edit</a></td>
                                    <td><a href="{{ route('get.delete.shipping.address', ['id' => $shippingAddress->internal_id]) }}">Delete</a></td>
                                    <td>@if($shippingAddress->default){{'Default address'}}@else <a href="{{ route('get.update.shipping.address', ['internalId' => $shippingAddress->internal_id]) }}">Make default address</a>@endif</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                    <p>You have not added any shipping address yet.</p>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
