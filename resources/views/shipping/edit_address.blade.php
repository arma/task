@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			@if (count($errors) > 0)
				<div class="col-md-8 col-md-offset-2">
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
				</div>
			@endif
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					<div class="panel-heading">{{ trans('shipping.edit address') }}</div>

					<div class="panel-body">
						<form class="form-horizontal" role="form" method="POST" action="{{ route('post.update.shipping.address') }}">
							{{ csrf_field() }}

							<div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
								<label for="name" class="col-md-4 control-label">Country</label>

								<div class="col-md-6">
									<input id="name" type="text" class="form-control" name="country" value="{{ old('country', $shippingAddress->country) }}" required autofocus>

									@if ($errors->has('country'))
										<span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
								<label for="name" class="col-md-4 control-label">City</label>

								<div class="col-md-6">
									<input id="name" type="text" class="form-control" name="city" value="{{ old('city', $shippingAddress->city) }}" required autofocus>

									@if ($errors->has('city'))
										<span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('zip_code') ? ' has-error' : '' }}">
								<label for="name" class="col-md-4 control-label">Zip code</label>

								<div class="col-md-6">
									<input id="name" type="text" class="form-control" name="zip_code" value="{{ old('zip_code', $shippingAddress->zip_code) }}" required autofocus>

									@if ($errors->has('zip_code'))
										<span class="help-block">
                                        <strong>{{ $errors->first('zip_code') }}</strong>
                                    </span>
									@endif
								</div>
							</div>

							<div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
								<label for="name" class="col-md-4 control-label">Street</label>

								<div class="col-md-6">
									<input id="name" type="text" class="form-control" name="street" value="{{ old('street', $shippingAddress->street) }}" required autofocus>

									@if ($errors->has('street'))
										<span class="help-block">
                                        <strong>{{ $errors->first('street') }}</strong>
                                    </span>
									@endif
								</div>
							</div>

							@if($shippingAddressesCount > 1 && !$shippingAddress->default)
								<div class="form-group">
									<div class="col-md-6 col-md-offset-4">
										<div class="checkbox">
											<label>
												<input type="checkbox" name="default" {{ old('default', 0) ? 'checked' : '' }}> Make default address
											</label>
										</div>
									</div>
								</div>
							@endif


							<div class="form-group">
								<div class="col-md-6 col-md-offset-4">
									<button type="submit" class="btn btn-primary">
										{{ trans('shipping.edit address button') }}
									</button>
								</div>
							</div>
							<input type="hidden" name="internal_id" value="{{ $shippingAddress->internal_id }}">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection